import re
from string import ascii_lowercase

import requests
from bs4 import BeautifulSoup

URL = "https://www.oberlin.edu/views/ajax"
NAME_LETTERS = set(ascii_lowercase + ' ')
MAILBOX_REGEX = re.compile(r"OCMR \d{4}")
PHONE_REGEX = re.compile(r"\d{3}-\d{3}-\d{4}")


def look_up(query):
    data = {
        "combine": query,
        "view_name": "campus_directory",
        "view_display_id": "campus_directory_listing_block",
    }

    response = requests.post(URL, data=data)
    return parse(response.json()[1]["data"])


def parse(response):
    soup = BeautifulSoup(response, "html.parser")
    table = soup.find("div", class_="view-content")
    if table is None:
        return
    for entry in table.find_all("article"):
        yield person(entry)


def person(entry):
    """
    Get a person. Name and type are guaranteed. All others optional.

    Other possible keys:
      - "mail"
      - "phone"
      - "title"
      - "email"
    """
    result = {"name": entry.h2.contents[0], "type": entry.find("div", class_="header-tag").contents[0]}

    lower_contents = entry.find("div", class_="basic-grid")
    email = lower_contents.find("a")
    if email:
        result["email"] = email["href"].partition(":")[2]

    possible_mail = lower_contents.find("div").text.strip()
    if MAILBOX_REGEX.match(possible_mail):
        result["mail"] = possible_mail
    else:
        result["title"] = possible_mail

    phone_number = PHONE_REGEX.search(lower_contents.text)
    if phone_number:
        result["phone"] = phone_number[0]

    return result


def clean(name):
    return ''.join(l for l in name.lower() if l in NAME_LETTERS)


def username(name):
    first, *_, last = clean(name).split()
    return first[0] + last[:7]
