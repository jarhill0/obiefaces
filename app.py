from flask import Flask, render_template, request

from lookup import look_up, username

app = Flask(__name__)


@app.route("/", methods=["GET"])
def index():
    return render_template("index.html")


@app.route("/search", methods=["GET"])
def search():
    query = request.values.get("q", "")
    results = look_up(query)
    return render_template(
        "results.html", results=results, query=query, username=username
    )


if __name__ == "__main__":
    app.run()
